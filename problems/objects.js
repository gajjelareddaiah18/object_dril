
function values(obj1) {
    let allValues = [];
    for (let key in obj1) {
        let value = obj1[key];
        allValues.push(value);
    }
    return allValues;
}
module.exports={
    values
}

