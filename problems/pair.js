
function pair(obj) {
    const keyValuePairLists = [];

    for (let key in obj) {
        let value=obj[key]
        keyValuePairLists.push(key ,value);
    }

    return keyValuePairLists;
}



module.exports={
    pair
}
